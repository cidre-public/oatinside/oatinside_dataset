package pg.unitarytests;

import android.app.Activity;
import android.os.Bundle;

public class TestLauncher extends Activity {

    public int intField = 6565;
    public char charField = '1';
    public long longField = (long) 2525;
    public byte byteField = (byte) 42;
    public boolean booleanField = false;
    public double doubleField = 0.442;
    public float floatField = (float) 0.12;

    /********************/
    /* Method behaviour */
    /********************/

    public int testInvokeReturnInt(int intVar) { return intVar; }
    public char testInvokeReturnChar(char charVar) { return charVar; }
    public long testInvokeReturnLong(long longVar) { return longVar; }
    public byte testInvokeReturnByte(byte byteVar) { return byteVar; }
    public boolean testInvokeReturnBoolean(boolean booleanVar) { return booleanVar; }
    public double testInvokeReturnDouble(double doubleVar) { return doubleVar; }
    public float testInvokeReturnFloat(float floatVar) { return floatVar; }
    public int testInvokeManyIntArgs(int arg1,int arg2,int arg3,int arg4,int arg5,int arg6,int arg7,int arg8,int arg9,int arg10,int arg11) {
        return arg1+arg2+arg3+arg4+arg5+arg6+arg7+arg8+arg9+arg10+arg11;}
    public long testSubInvokeReturn() {return testInvokeReturnInt(1) + testInvokeReturnLong(2) + testInvokeReturnByte((byte)3);}

    /***********************/
    /* Condition behaviour */
    /***********************/

    public boolean testConditionVariableEq(int i) {if( i == 42) return true; else return false; }
    public boolean testConditionVariableInfEq(int i) { if( i <= 42) return true; else return false; }
    public boolean testConditionVariableSup(int i) { if( i > 42) return true; else return false; }
    public boolean testConditionArrEq(int[] i) {if( i[0] == 42) return true; else return false; }
    public boolean testConditionArrInfEq(int[] i) { if( i[0] <= 42) return true; else return false; }
    public boolean testConditionArrSup(int[] i) { if( i[0] > 42) return true; else return false; }
    public boolean testConditionObjectEq() {if( intField == 42) return true; else return false; }
    public boolean testConditionObjectInfEq() { if( intField <= 42) return true; else return false; }
    public boolean testConditionObjectSup() { if( intField > 42) return true; else return false; }

    /************************/
    /* Allocation behaviour */
    /************************/

    public class AllocatedClass {int varI; char varC; long varL; byte varByte; boolean varB;
        double varD; float varF;}
    public AllocatedClass testAllocateObject() {return new AllocatedClass();}
    public void testAllocateVariable() {int varI; char varC; long varL; byte varByte; boolean varB;
    double varD; float varF;}
    public void testAllocateArray() {int[] varI={}; char[] varC={}; long[] varL={}; byte[] varByte={}; boolean[] varB={};
        double[] varD={}; float[] varF={};}

    /********************/
    /* Access behaviour */
    /********************/

    public int testAccessObjectInt() { int tmpInt; tmpInt=intField;intField=3000;return tmpInt;}
    public char testAccessObjectChar() {char tmpChar; tmpChar=charField;charField='o';return tmpChar;}
    public long testAccessObjectLong() {long tmpLong; tmpLong=longField;longField=2;return tmpLong;}
    public byte testAccessObjectByte() {byte tmpByte; tmpByte=byteField;byteField=12;return tmpByte;}
    public boolean testAccessObjectBoolean() {boolean tmpBoolean; tmpBoolean=booleanField;booleanField=false;return tmpBoolean;}
    public double testAccessObjectDouble() {double tmpDouble; tmpDouble=doubleField;doubleField=1.568;return tmpDouble;}
    public float testAccessObjectFloat() {float tmpFloat; tmpFloat=floatField;floatField=(float)123.5;return tmpFloat;}
    public int testAccessVariableInt(int intVar) { int tmpInt; tmpInt=intVar;intVar=3000;return tmpInt;}
    public char testAccessVariableChar(char charVar) {char tmpChar; tmpChar=charVar;charVar='o';return tmpChar;}
    public long testAccessVariableLong(long longVar) {long tmpLong; tmpLong=longVar;longVar=2;return tmpLong;}
    public byte testAccessVariableByte(byte byteVar) {byte tmpByte; tmpByte=byteVar;byteVar=12;return tmpByte;}
    public boolean testAccessVariableBoolean(boolean booleanVar) {boolean tmpBoolean; tmpBoolean=booleanVar;booleanVar=false;return tmpBoolean;}
    public double testAccessVariableDouble(double doubleVar) {double tmpDouble; tmpDouble=doubleVar;doubleVar=1.568;return tmpDouble;}
    public float testAccessVariableFloat(float floatVar) {float tmpFloat; tmpFloat=floatVar;floatVar=(float)123.5;return tmpFloat;}
    public int testAccessArrInt(int[] intVar) { int tmpInt; tmpInt=intVar[0];intVar[0]=3000;return tmpInt;}
    public char testAccessArrChar(char[] charVar) {char tmpChar; tmpChar=charVar[0];charVar[0]='o';return tmpChar;}
    public long testAccessArrLong(long[] longVar) {long tmpLong; tmpLong=longVar[0];longVar[0]=2;return tmpLong;}
    public byte testAccessArrByte(byte[] byteVar) {byte tmpByte; tmpByte=byteVar[0];byteVar[0]=12;return tmpByte;}
    public boolean testAccessArrBoolean(boolean[] booleanVar) {boolean tmpBoolean; tmpBoolean=booleanVar[0];booleanVar[0]=false;return tmpBoolean;}
    public double testAccessArrDouble(double[] doubleVar) {double tmpDouble; tmpDouble=doubleVar[0];doubleVar[0]=1.568;return tmpDouble;}
    public float testAccessArrFloat(float[] floatVar) {float tmpFloat; tmpFloat=floatVar[0];floatVar[0]=(float)123.5;return tmpFloat;}

    /************************/
    /* Operations behaviour */
    /************************/

    public int testOperationsObjectInt() {return intField + 1;}
    public char testOperationsObjectChar() {return (char)((int)(charField) + 2);}
    public long testOperationsObjectLong() {return longField - 3;}
    public byte testOperationsObjectByte() {return (byte)(byteField + (byte)4);}
    public boolean testOperationsObjectBoolean() {return booleanField ^ false;}
    public double testOperationsObjectDouble() {return doubleField * 2;}
    public float testOperationsObjectFloat() {return floatField / 40;}
    public int testOperationsVariableInt(int intVar) {
        return intVar + 1;
    }
    public char testOperationsVariableChar(char charVar) { return (char)((int)(charVar)+2); }
    public long testOperationsVariableLong(long longVar) { return longVar+89; }
    public byte testOperationsVariableByte(byte byteVar) { return (byte)(byteVar+(byte)789); }
    public boolean testOperationsVariableBoolean(boolean booleanVar) {
        return booleanVar ^ true;
    }
    public double testOperationsVariableDouble(double doubleVar) {
        return doubleVar*78.4;
    }
    public float testOperationsVariableFloat(float floatVar) {
        return floatVar/789;
    }
    public int testOperationsArrInt(int[] intVar) {
        return intVar[0] + 1;
    }
    public char testOperationsArrChar(char[] charVar) { return (char)((int)(charVar[0])+2); }
    public long testOperationsArrLong(long[] longVar) { return longVar[0]+89; }
    public byte testOperationsArrByte(byte[] byteVar) { return (byte)(byteVar[0]+(byte)789); }
    public boolean testOperationsArrBoolean(boolean[] booleanVar) {
        return booleanVar[0] ^ true;
    }
    public double testOperationsArrDouble(double[] doubleVar) {
        return doubleVar[0]*78.4;
    }
    public float testOperationsArrFloat(float[] floatVar) {
        return floatVar[0]/789;
    }

    /********************/
    /* Typing behaviour */
    /********************/

    public class Toto {}
    public class Tata extends Toto {}
    public class Tutu extends Toto {}
    public boolean testCheckType(Toto t) {return t instanceof Tata;}
    public Tata testCastType(Toto t) {return (Tata)t;}

    /***********************/
    /* Exception behaviour */
    /***********************/

    public void testThrow() {
        throw new IllegalArgumentException("Testing throw");
    }
    public void testNoCatch() {
        testThrow();
    }
    public void testThrowCatch() {
        try { testNoCatch(); } catch (Exception e) {}
    }

    /*********************/
    /* Monitor behaviour */
    /*********************/

    public void testMonitor(int i) {synchronized(this){try{Thread.sleep(i);} catch(InterruptedException e){}}}

    /************/
    /* Launcher */
    /************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mysimplelayout);

        /* Method behaviour */
        testInvokeReturnInt(42);
        testInvokeReturnChar('a');
        testInvokeReturnLong((long)1337);
        testInvokeReturnByte((byte)23);
        testInvokeReturnBoolean(true);
        testInvokeReturnDouble(0.23);
        testInvokeReturnFloat((float)0.21);
        testInvokeManyIntArgs(1,2,3,4,5,6,7,8,9,10,11);
        testSubInvokeReturn();

        /* Condition behaviour */
        testConditionVariableEq(intField);
        testConditionVariableInfEq(intField);
        testConditionVariableSup(intField);
        testConditionArrEq(new int[]{42});
        testConditionArrInfEq(new int[]{42});
        testConditionArrSup(new int[]{42});
        testConditionObjectEq();
        testConditionObjectInfEq();
        testConditionObjectSup();

        /* Allocation behaviour */
        testAllocateObject();
        testAllocateVariable();
        testAllocateArray();

        /* Access behaviour */
        testAccessObjectInt();
        testAccessObjectChar();
        testAccessObjectLong();
        testAccessObjectByte();
        testAccessObjectBoolean();
        testAccessObjectDouble();
        testAccessObjectFloat();
        testAccessVariableInt(456);
        testAccessVariableChar('t');
        testAccessVariableLong(7896);
        testAccessVariableByte((byte)12);
        testAccessVariableBoolean(true);
        testAccessVariableDouble(12.5);
        testAccessVariableFloat((float)123.34);
        testAccessArrInt(new int[]{1});
        testAccessArrChar(new char[]{'a'});
        testAccessArrLong(new long[]{1337});
        testAccessArrByte(new byte[]{42});
        testAccessArrBoolean(new boolean[]{true,false});
        testAccessArrDouble(new double[]{12.5});
        testAccessArrFloat(new float[]{(float)5.6});

        /* Operations behaviour */
        testOperationsObjectInt();
        testOperationsObjectChar();
        testOperationsObjectLong();
        testOperationsObjectByte();
        testOperationsObjectBoolean();
        testOperationsObjectDouble();
        testOperationsObjectFloat();
        testOperationsVariableInt(456);
        testOperationsVariableChar('t');
        testOperationsVariableLong(7896);
        testOperationsVariableByte((byte)12);
        testOperationsVariableBoolean(true);
        testOperationsVariableDouble(12.5);
        testOperationsVariableFloat((float)123.34);
        testOperationsArrInt(new int[]{1});
        testOperationsArrChar(new char[]{'a'});
        testOperationsArrLong(new long[]{1337});
        testOperationsArrByte(new byte[]{42});
        testOperationsArrBoolean(new boolean[]{true,false});
        testOperationsArrDouble(new double[]{12.5});
        testOperationsArrFloat(new float[]{(float)5.6});

        /* Typing behaviour */
        testCheckType(new Tutu());
        testCastType(new Tata());

        /* Exception behaviour */
        testThrowCatch();

        /* Monitor behaviour */
        new Thread(new Runnable() {public void run() {testMonitor(2000);}}).start();
        new Thread(new Runnable() {public void run() {testMonitor(1);}}).start();
    }
}
