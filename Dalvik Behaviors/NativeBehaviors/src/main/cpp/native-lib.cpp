#include <jni.h>

#define GET_CLS jclass cls = env->GetObjectClass(instance)


/* Usage:
 * GET_FIELD(int, Int, "I"); for getting intField
 * GET_FIELD(char, Char, "C");
 * GET_FIELD(long, Long, "J");
 * GET_FIELD(byte, Byte, "B");
 * GET_FIELD(boolean, Boolean, "Z");
 * GET_FIELD(double, Double, "D");
 * GET_FIELD(float, Float, "F");
 * */
#define GET_FIELD(type, Type, sig) \
    jfieldID fid = env->GetFieldID(cls, #type "Field", sig); \
    j##type type##Field = env->Get##Type##Field(instance, fid);


/********************/
/* Method behaviour */
/********************/

extern "C" JNIEXPORT jint JNICALL Java_pg_nativetests_TestLauncher_testInvokeReturnInt(JNIEnv *env, jobject instance, jint intVar) { return intVar; }
extern "C" JNIEXPORT jchar JNICALL Java_pg_nativetests_TestLauncher_testInvokeReturnChar(JNIEnv *env, jobject instance, jchar charVar) { return charVar; }
extern "C" JNIEXPORT jlong JNICALL Java_pg_nativetests_TestLauncher_testInvokeReturnLong(JNIEnv *env, jobject instance, jlong longVar) { return longVar; }
extern "C" JNIEXPORT jbyte JNICALL Java_pg_nativetests_TestLauncher_testInvokeReturnByte(JNIEnv *env, jobject instance, jbyte byteVar) { return byteVar; }
extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testInvokeReturnBoolean(JNIEnv *env, jobject instance, jboolean booleanVar) { return booleanVar; }
extern "C" JNIEXPORT jdouble JNICALL Java_pg_nativetests_TestLauncher_testInvokeReturnDouble(JNIEnv *env, jobject instance,jdouble doubleVar) { return doubleVar; }
extern"C" JNIEXPORT jfloat JNICALL Java_pg_nativetests_TestLauncher_testInvokeReturnFloat(JNIEnv *env, jobject instance, jfloat floatVar) { return floatVar; }
extern "C" JNIEXPORT jint JNICALL Java_pg_nativetests_TestLauncher_testInvokeManyIntArgs(JNIEnv *env, jobject instance, jint arg1,
                                                       jint arg2, jint arg3, jint arg4, jint arg5, jint arg6, jint arg7, jint arg8, jint arg9,
                                                       jint arg10, jint arg11) { return  arg1+arg2+arg3+arg4+arg5+arg6+arg7+arg8+arg9+arg10+arg10+arg11;}
extern "C" JNIEXPORT jlong JNICALL Java_pg_nativetests_TestLauncher_testSubInvokeReturn(JNIEnv *env, jobject instance) { GET_CLS; jmethodID mid; jlong res = 0;
    mid = env->GetMethodID(cls, "testInvokeReturnInt", "(I)I"); res += env->CallIntMethod(instance, mid, (jint)1);
    mid = env->GetMethodID(cls, "testInvokeReturnLong", "(J)J"); res += env->CallLongMethod(instance, mid, (jlong)1);
    mid = env->GetMethodID(cls, "testInvokeReturnByte", "(B)B"); res += env->CallByteMethod(instance, mid, (jbyte)3);
    return res; }

/***********************/
/* Condition behaviour */
/***********************/

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testConditionVariableEq(JNIEnv *env, jobject instance, jint i) {
    if( i == 42 ) return true; else return false; }

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testConditionVariableInfEq(JNIEnv *env, jobject instance, jint i) {
    if( i <= 42 ) return true; else return false; }

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testConditionVariableSup(JNIEnv *env, jobject instance, jint i) {
    if( i > 42 ) return true; else return false; }

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testConditionArrEq(JNIEnv *env, jobject instance, jintArray i_) {
    jint *i = env->GetIntArrayElements(i_, NULL);
    if( i[0] == 42 ) {env->ReleaseIntArrayElements(i_, i, 0);return true;} else {env->ReleaseIntArrayElements(i_, i, 0);return false;}}

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testConditionArrInfEq(JNIEnv *env, jobject instance, jintArray i_) {
    jint *i = env->GetIntArrayElements(i_, NULL);
    if( i[0] <= 42 ) {env->ReleaseIntArrayElements(i_, i, 0);return true;} else {env->ReleaseIntArrayElements(i_, i, 0);return false;}}

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testConditionArrSup(JNIEnv *env, jobject instance, jintArray i_) {
    jint *i = env->GetIntArrayElements(i_, NULL);
    if( i[0] > 42 ) {env->ReleaseIntArrayElements(i_, i, 0);return true;} else {env->ReleaseIntArrayElements(i_, i, 0);return false;}}

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testConditionObjectEq(JNIEnv *env, jobject instance) {
    GET_CLS; GET_FIELD(int, Int, "I"); if(intField == 42 ) return true; else return false;}

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testConditionObjectInfEq(JNIEnv *env, jobject instance) {
    GET_CLS; GET_FIELD(int, Int, "I"); if(intField <= 42 ) return true; else return false;}

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testConditionObjectSup(JNIEnv *env, jobject instance) {
    GET_CLS; GET_FIELD(int, Int, "I"); if(intField > 42 ) return true; else return false;}

/************************/
/* Allocation behaviour */
/************************/

extern "C" JNIEXPORT jobject JNICALL Java_pg_nativetests_TestLauncher_testAllocateObject(JNIEnv *env, jobject instance) {
    jclass cls = env->FindClass("pg/nativetests/TestLauncher$AllocatedClass"); return env->AllocObject(cls);}

extern "C" JNIEXPORT void JNICALL Java_pg_nativetests_TestLauncher_testAllocateVariable(JNIEnv *env, jobject instance) {
    jint varI; jchar varC; jlong varL; jbyte varByte; jboolean varB; jdouble varD; jfloat varF;}

extern "C" JNIEXPORT void JNICALL Java_pg_nativetests_TestLauncher_testAllocateArray(JNIEnv *env, jobject instance) {
    jintArray varI = env->NewIntArray(1);jcharArray varC = env->NewCharArray(1);jlongArray varL = env->NewLongArray(1);jbyteArray varByte = env->NewByteArray(1);
    jbooleanArray varB = env->NewBooleanArray(1);jdoubleArray varD = env->NewDoubleArray(1);jfloatArray varF = env->NewFloatArray(1);}

/********************/
/* Access behaviour */
/********************/

extern "C" JNIEXPORT jint JNICALL Java_pg_nativetests_TestLauncher_testAccessObjectInt(JNIEnv *env, jobject instance) {
    GET_CLS;GET_FIELD(int, Int, "I");jint tmp = intField;env->SetIntField(instance, fid, 3000);return tmp;}

extern "C" JNIEXPORT jchar JNICALL Java_pg_nativetests_TestLauncher_testAccessObjectChar(JNIEnv *env, jobject instance) {
    GET_CLS;GET_FIELD(char, Char, "C");jchar tmp = charField;env->SetCharField(instance, fid, 'o');return tmp;}

extern "C" JNIEXPORT jlong JNICALL Java_pg_nativetests_TestLauncher_testAccessObjectLong(JNIEnv *env, jobject instance) {
    GET_CLS;GET_FIELD(long, Long, "J");jlong tmp = longField;env->SetLongField(instance, fid, 2);return tmp;}

extern "C" JNIEXPORT jbyte JNICALL Java_pg_nativetests_TestLauncher_testAccessObjectByte(JNIEnv *env, jobject instance) {
    GET_CLS;GET_FIELD(byte, Byte, "B");jbyte tmp = byteField;env->SetByteField(instance, fid, 12);return tmp;}

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testAccessObjectBoolean(JNIEnv *env, jobject instance) {
    GET_CLS;GET_FIELD(boolean, Boolean, "Z");jboolean tmp = booleanField;env->SetBooleanField(instance, fid, false);return tmp;}

extern "C" JNIEXPORT jdouble JNICALL Java_pg_nativetests_TestLauncher_testAccessObjectDouble(JNIEnv *env, jobject instance) {
    GET_CLS;GET_FIELD(double, Double, "D");jdouble tmp = doubleField;env->SetDoubleField(instance, fid, 1.568);return tmp;}

extern "C" JNIEXPORT jfloat JNICALL Java_pg_nativetests_TestLauncher_testAccessObjectFloat(JNIEnv *env, jobject instance) {
    GET_CLS;GET_FIELD(float, Float, "F");jfloat tmp = floatField;env->SetFloatField(instance, fid, 123.5);return tmp;}

extern "C" JNIEXPORT jint JNICALL Java_pg_nativetests_TestLauncher_testAccessVariableInt(JNIEnv *env, jobject instance, jint intVar) {
    jint tmp=intVar; intVar=3000;return tmp;}

extern "C" JNIEXPORT jchar JNICALL Java_pg_nativetests_TestLauncher_testAccessVariableChar(JNIEnv *env, jobject instance, jchar charVar) {
    jchar tmp=charVar; charVar=3000;return tmp;}

extern "C" JNIEXPORT jlong JNICALL Java_pg_nativetests_TestLauncher_testAccessVariableLong(JNIEnv *env, jobject instance, jlong longVar) {
    jlong tmp=longVar; longVar=3000;return tmp;}

extern "C" JNIEXPORT jbyte JNICALL Java_pg_nativetests_TestLauncher_testAccessVariableByte(JNIEnv *env, jobject instance, jbyte byteVar) {
    jbyte tmp=byteVar; byteVar=3000;return tmp;}

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testAccessVariableBoolean(JNIEnv *env, jobject instance, jboolean booleanVar) {
    jboolean tmp=booleanVar; booleanVar=3000;return tmp;}

extern "C" JNIEXPORT jdouble JNICALL Java_pg_nativetests_TestLauncher_testAccessVariableDouble(JNIEnv *env, jobject instance, jdouble doubleVar) {
    jdouble tmp=doubleVar; doubleVar=3000;return tmp;}

extern "C" JNIEXPORT jfloat JNICALL Java_pg_nativetests_TestLauncher_testAccessVariableFloat(JNIEnv *env, jobject instance, jfloat floatVar) {
    jfloat tmp=floatVar; floatVar=3000;return tmp;}

extern "C" JNIEXPORT jint JNICALL Java_pg_nativetests_TestLauncher_testAccessArrInt(JNIEnv *env, jobject instance, jintArray intVar_) {
    jint *intVar = env->GetIntArrayElements(intVar_, NULL);jint tmp = intVar[0];intVar[0] = 3000;env->ReleaseIntArrayElements(intVar_, intVar, 0);return tmp;}

extern "C" JNIEXPORT jchar JNICALL Java_pg_nativetests_TestLauncher_testAccessArrChar(JNIEnv *env, jobject instance, jcharArray charVar_) {
    jchar *charVar = env->GetCharArrayElements(charVar_, NULL);jchar tmp = charVar[0];env->ReleaseCharArrayElements(charVar_, charVar, 0);return tmp;}

extern "C" JNIEXPORT jlong JNICALL Java_pg_nativetests_TestLauncher_testAccessArrLong(JNIEnv *env, jobject instance, jlongArray longVar_) {
    jlong *longVar = env->GetLongArrayElements(longVar_, NULL);jlong tmp = longVar[0];env->ReleaseLongArrayElements(longVar_, longVar, 0);return tmp;}

extern "C" JNIEXPORT jbyte JNICALL Java_pg_nativetests_TestLauncher_testAccessArrByte(JNIEnv *env, jobject instance, jbyteArray byteVar_) {
    jbyte *byteVar = env->GetByteArrayElements(byteVar_, NULL);jbyte tmp = byteVar[0];env->ReleaseByteArrayElements(byteVar_, byteVar, 0);return tmp;}

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testAccessArrBoolean(JNIEnv *env, jobject instance, jbooleanArray booleanVar_) {
    jboolean *booleanVar = env->GetBooleanArrayElements(booleanVar_, NULL);jboolean tmp = booleanVar[0];env->ReleaseBooleanArrayElements(booleanVar_, booleanVar, 0);return tmp;}

extern "C" JNIEXPORT jdouble JNICALL Java_pg_nativetests_TestLauncher_testAccessArrDouble(JNIEnv *env, jobject instance, jdoubleArray doubleVar_) {
    jdouble *doubleVar = env->GetDoubleArrayElements(doubleVar_, NULL);jdouble tmp = doubleVar[0];env->ReleaseDoubleArrayElements(doubleVar_, doubleVar, 0);return tmp;}

extern "C" JNIEXPORT jfloat JNICALL Java_pg_nativetests_TestLauncher_testAccessArrFloat(JNIEnv *env, jobject instance, jfloatArray floatVar_) {
    jfloat *floatVar = env->GetFloatArrayElements(floatVar_, NULL);float tmp = floatVar[0];env->ReleaseFloatArrayElements(floatVar_, floatVar, 0); return tmp;}

/************************/
/* Operations behaviour */
/************************/

extern "C" JNIEXPORT jint JNICALL Java_pg_nativetests_TestLauncher_testOperationsObjectInt(JNIEnv *env, jobject instance) {
    GET_CLS;GET_FIELD(int,Int,"I");return intField+1; }

extern "C" JNIEXPORT jchar JNICALL Java_pg_nativetests_TestLauncher_testOperationsObjectChar(JNIEnv *env, jobject instance) {
    GET_CLS;GET_FIELD(char,Char,"C");return charField+2; }

extern "C" JNIEXPORT jlong JNICALL Java_pg_nativetests_TestLauncher_testOperationsObjectLong(JNIEnv *env, jobject instance) {
    GET_CLS;GET_FIELD(long,Long,"J");return longField-3; }

extern "C" JNIEXPORT jbyte JNICALL Java_pg_nativetests_TestLauncher_testOperationsObjectByte(JNIEnv *env, jobject instance) {
    GET_CLS;GET_FIELD(byte,Byte,"B");return byteField+4; }

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testOperationsObjectBoolean(JNIEnv *env, jobject instance) {
    GET_CLS;GET_FIELD(boolean,Boolean,"Z");return booleanField^ false; }

extern "C" JNIEXPORT jdouble JNICALL Java_pg_nativetests_TestLauncher_testOperationsObjectDouble(JNIEnv *env, jobject instance) {
    GET_CLS;GET_FIELD(double,Double,"D");return doubleField*2; }

extern "C" JNIEXPORT jfloat JNICALL Java_pg_nativetests_TestLauncher_testOperationsObjectFloat(JNIEnv *env, jobject instance) {
    GET_CLS;GET_FIELD(float,Float,"F");return floatField/40; }

extern "C" JNIEXPORT jint JNICALL Java_pg_nativetests_TestLauncher_testOperationsVariableInt(JNIEnv *env, jobject instance, jint intVar) {return intVar+1;}

extern "C" JNIEXPORT jchar JNICALL Java_pg_nativetests_TestLauncher_testOperationsVariableChar(JNIEnv *env, jobject instance, jchar charVar) {return charVar+2;}

extern "C" JNIEXPORT jlong JNICALL Java_pg_nativetests_TestLauncher_testOperationsVariableLong(JNIEnv *env, jobject instance, jlong longVar) {return longVar+89;}

extern "C" JNIEXPORT jbyte JNICALL Java_pg_nativetests_TestLauncher_testOperationsVariableByte(JNIEnv *env, jobject instance, jbyte byteVar) {return byteVar + 789;}

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testOperationsVariableBoolean(JNIEnv *env, jobject instance, jboolean booleanVar) {return booleanVar ^ true;}

extern "C" JNIEXPORT jdouble JNICALL Java_pg_nativetests_TestLauncher_testOperationsVariableDouble(JNIEnv *env, jobject instance, jdouble doubleVar) {return doubleVar*78.4;}

extern "C" JNIEXPORT jfloat JNICALL Java_pg_nativetests_TestLauncher_testOperationsVariableFloat(JNIEnv *env, jobject instance, jfloat floatVar) {return floatVar/789;}

extern "C" JNIEXPORT jint JNICALL Java_pg_nativetests_TestLauncher_testOperationsArrInt(JNIEnv *env, jobject instance, jintArray intVar_) {
    jint *intVar = env->GetIntArrayElements(intVar_, NULL);jint res = intVar[0];env->ReleaseIntArrayElements(intVar_, intVar, 0);return res+1;}

extern "C" JNIEXPORT jchar JNICALL Java_pg_nativetests_TestLauncher_testOperationsArrChar(JNIEnv *env, jobject instance, jcharArray charVar_) {
    jchar *charVar = env->GetCharArrayElements(charVar_, NULL);jchar res = charVar[0];env->ReleaseCharArrayElements(charVar_, charVar, 0);return res+2;}

extern "C" JNIEXPORT jlong JNICALL Java_pg_nativetests_TestLauncher_testOperationsArrLong(JNIEnv *env, jobject instance, jlongArray longVar_) {
    jlong *longVar = env->GetLongArrayElements(longVar_, NULL);jlong res = longVar[0];env->ReleaseLongArrayElements(longVar_, longVar, 0);return res + 89;}

extern "C" JNIEXPORT jbyte JNICALL Java_pg_nativetests_TestLauncher_testOperationsArrByte(JNIEnv *env, jobject instance, jbyteArray byteVar_) {
    jbyte *byteVar = env->GetByteArrayElements(byteVar_, NULL);jbyte res = byteVar[0];env->ReleaseByteArrayElements(byteVar_, byteVar, 0);return res + 789;}

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testOperationsArrBoolean(JNIEnv *env, jobject instance, jbooleanArray booleanVar_) {
    jboolean *booleanVar = env->GetBooleanArrayElements(booleanVar_, NULL);jboolean res = booleanVar[0];env->ReleaseBooleanArrayElements(booleanVar_, booleanVar, 0);return res ^ true;}

extern "C" JNIEXPORT jdouble JNICALL Java_pg_nativetests_TestLauncher_testOperationsArrDouble(JNIEnv *env, jobject instance, jdoubleArray doubleVar_) {
    jdouble *doubleVar = env->GetDoubleArrayElements(doubleVar_, NULL);jdouble res = doubleVar[0];env->ReleaseDoubleArrayElements(doubleVar_, doubleVar, 0);return res * 78.4;}

extern "C" JNIEXPORT jfloat JNICALL Java_pg_nativetests_TestLauncher_testOperationsArrFloat(JNIEnv *env, jobject instance, jfloatArray floatVar_) {
    jfloat *floatVar = env->GetFloatArrayElements(floatVar_, NULL);jfloat res = floatVar[0];env->ReleaseFloatArrayElements(floatVar_, floatVar, 0);return res / 789;}

/********************/
/* Typing behaviour */
/********************/

extern "C" JNIEXPORT jboolean JNICALL Java_pg_nativetests_TestLauncher_testCheckType(JNIEnv *env, jobject instance, jobject t) {
    jclass cls = env->FindClass("pg/nativetests/TestLauncher$Tata"); return env->IsInstanceOf(t, cls);}

extern "C" JNIEXPORT jobject JNICALL Java_pg_nativetests_TestLauncher_testCastType(JNIEnv *env, jobject instance, jobject t) {
    return t;}

/***********************/
/* Exception behaviour */
/***********************/

extern "C" JNIEXPORT void JNICALL Java_pg_nativetests_TestLauncher_testThrow(JNIEnv *env, jobject instance) {
    jclass cls = env->FindClass("java/lang/IllegalArgumentException");env->ThrowNew(cls, "Testing throw");}

extern "C" JNIEXPORT void JNICALL Java_pg_nativetests_TestLauncher_testNoCatch(JNIEnv *env, jobject instance) {
    GET_CLS;jmethodID mid= env->GetMethodID(cls, "testThrow", "()V");env->CallVoidMethod(instance, mid, (jint)1);}

extern "C" JNIEXPORT void JNICALL Java_pg_nativetests_TestLauncher_testThrowCatch(JNIEnv *env, jobject instance) {
    GET_CLS;jmethodID mid= env->GetMethodID(cls, "testNoCatch", "()V");env->CallVoidMethod(instance, mid, (jint)1);if(env->ExceptionCheck() == JNI_TRUE)env->ExceptionClear();}

/*********************/
/* Monitor behaviour */
/*********************/

extern "C" JNIEXPORT void JNICALL Java_pg_nativetests_TestLauncher_testMonitor(JNIEnv *env, jobject instance, jint i) {
    env->MonitorEnter(instance);

    jclass cls = env->FindClass("java/lang/Thread");
    jmethodID mid = env->GetStaticMethodID(cls, "sleep", "(J)V");
    env->CallStaticVoidMethod(cls, mid, i);

    if(env->ExceptionCheck() == JNI_TRUE)
        env->ExceptionClear();

    env->MonitorExit(instance);
}