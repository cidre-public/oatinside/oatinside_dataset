package pg.nativetests;

import android.app.Activity;
import android.os.Bundle;

public class TestLauncher extends Activity {

    static {
        System.loadLibrary("native-lib");
    }

    public int intField = 6565;
    public char charField = '1';
    public long longField = (long) 2525;
    public byte byteField = (byte) 42;
    public boolean booleanField = false;
    public double doubleField = 0.442;
    public float floatField = (float) 0.12;

    /********************/
    /* Method behaviour */
    /********************/

    public native int testInvokeReturnInt(int intVar);
    public native char testInvokeReturnChar(char charVar);
    public native long testInvokeReturnLong(long longVar);
    public native byte testInvokeReturnByte(byte byteVar);
    public native boolean testInvokeReturnBoolean(boolean booleanVar);
    public native double testInvokeReturnDouble(double doubleVar);
    public native float testInvokeReturnFloat(float floatVar);
    public native int testInvokeManyIntArgs(int arg1,int arg2,int arg3,int arg4,int arg5,int arg6,int arg7,int arg8,int arg9,int arg10,int arg11);
    public native long testSubInvokeReturn();

    /***********************/
    /* Condition behaviour */
    /***********************/

    public native boolean testConditionVariableEq(int i);
    public native boolean testConditionVariableInfEq(int i);
    public native boolean testConditionVariableSup(int i);
    public native boolean testConditionArrEq(int[] i) ;
    public native boolean testConditionArrInfEq(int[] i);
    public native boolean testConditionArrSup(int[] i);
    public native boolean testConditionObjectEq();
    public native boolean testConditionObjectInfEq();
    public native boolean testConditionObjectSup();

    /************************/
    /* Allocation behaviour */
    /************************/

    public class AllocatedClass {int varI; char varC; long varL; byte varByte; boolean varB;
        double varD; float varF;}
    public native AllocatedClass testAllocateObject();
    public native void testAllocateVariable();
    public native void testAllocateArray();

    /********************/
    /* Access behaviour */
    /********************/

    public native int testAccessObjectInt();
    public native char testAccessObjectChar();
    public native long testAccessObjectLong();
    public native byte testAccessObjectByte();
    public native boolean testAccessObjectBoolean();
    public native double testAccessObjectDouble();
    public native float testAccessObjectFloat();
    public native int testAccessVariableInt(int intVar);
    public native char testAccessVariableChar(char charVar);
    public native long testAccessVariableLong(long longVar);
    public native byte testAccessVariableByte(byte byteVar);
    public native boolean testAccessVariableBoolean(boolean booleanVar);
    public native double testAccessVariableDouble(double doubleVar);
    public native float testAccessVariableFloat(float floatVar);
    public native int testAccessArrInt(int[] intVar);
    public native char testAccessArrChar(char[] charVar);
    public native long testAccessArrLong(long[] longVar);
    public native byte testAccessArrByte(byte[] byteVar);
    public native boolean testAccessArrBoolean(boolean[] booleanVar);
    public native double testAccessArrDouble(double[] doubleVar);
    public native float testAccessArrFloat(float[] floatVar);

    /************************/
    /* Operations behaviour */
    /************************/

    public native int testOperationsObjectInt();
    public native char testOperationsObjectChar();
    public native long testOperationsObjectLong();
    public native byte testOperationsObjectByte();
    public native boolean testOperationsObjectBoolean();
    public native double testOperationsObjectDouble();
    public native float testOperationsObjectFloat();
    public native int testOperationsVariableInt(int intVar);
    public native char testOperationsVariableChar(char charVar);
    public native long testOperationsVariableLong(long longVar);
    public native byte testOperationsVariableByte(byte byteVar);
    public native boolean testOperationsVariableBoolean(boolean booleanVar);
    public native double testOperationsVariableDouble(double doubleVar);
    public native float testOperationsVariableFloat(float floatVar);
    public native int testOperationsArrInt(int[] intVar);
    public native char testOperationsArrChar(char[] charVar);
    public native long testOperationsArrLong(long[] longVar);
    public native byte testOperationsArrByte(byte[] byteVar);
    public native boolean testOperationsArrBoolean(boolean[] booleanVar);
    public native double testOperationsArrDouble(double[] doubleVar);
    public native float testOperationsArrFloat(float[] floatVar);

    /********************/
    /* Typing behaviour */
    /********************/

    public class Toto {}
    public class Tata extends Toto {}
    public class Tutu extends Toto {}
    public native boolean testCheckType(Toto t);
    public native Tata testCastType(Toto t);

    /***********************/
    /* Exception behaviour */
    /***********************/

    public native void testThrow();
    public native void testNoCatch();
    public native void testThrowCatch();

    /*********************/
    /* Monitor behaviour */
    /*********************/

    public native void testMonitor(int i);

    /************/
    /* Launcher */
    /************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mysimplelayout);

        /* Method behaviour */
        testInvokeReturnInt(42);
        testInvokeReturnChar('a');
        testInvokeReturnLong((long)1337);
        testInvokeReturnByte((byte)23);
        testInvokeReturnBoolean(true);
        testInvokeReturnDouble(0.23);
        testInvokeReturnFloat((float)0.21);
        testInvokeManyIntArgs(1,2,3,4,5,6,7,8,9,10,11);
        testSubInvokeReturn();

        /* Condition behaviour */
        testConditionVariableEq(intField);
        testConditionVariableInfEq(intField);
        testConditionVariableSup(intField);
        testConditionArrEq(new int[]{42});
        testConditionArrInfEq(new int[]{42});
        testConditionArrSup(new int[]{42});
        testConditionObjectEq();
        testConditionObjectInfEq();
        testConditionObjectSup();

        /* Allocation behaviour */
        testAllocateObject();
        testAllocateVariable();
        testAllocateArray();

        /* Access behaviour */
        testAccessObjectInt();
        testAccessObjectChar();
        testAccessObjectLong();
        testAccessObjectByte();
        testAccessObjectBoolean();
        testAccessObjectDouble();
        testAccessObjectFloat();
        testAccessVariableInt(456);
        testAccessVariableChar('t');
        testAccessVariableLong(7896);
        testAccessVariableByte((byte)12);
        testAccessVariableBoolean(true);
        testAccessVariableDouble(12.5);
        testAccessVariableFloat((float)123.34);
        testAccessArrInt(new int[]{1});
        testAccessArrChar(new char[]{'a'});
        testAccessArrLong(new long[]{1337});
        testAccessArrByte(new byte[]{42});
        testAccessArrBoolean(new boolean[]{true,false});
        testAccessArrDouble(new double[]{12.5});
        testAccessArrFloat(new float[]{(float)5.6});

        /* Operations behaviour */
        testOperationsObjectInt();
        testOperationsObjectChar();
        testOperationsObjectLong();
        testOperationsObjectByte();
        testOperationsObjectBoolean();
        testOperationsObjectDouble();
        testOperationsObjectFloat();
        testOperationsVariableInt(456);
        testOperationsVariableChar('t');
        testOperationsVariableLong(7896);
        testOperationsVariableByte((byte)12);
        testOperationsVariableBoolean(true);
        testOperationsVariableDouble(12.5);
        testOperationsVariableFloat((float)123.34);
        testOperationsArrInt(new int[]{1});
        testOperationsArrChar(new char[]{'a'});
        testOperationsArrLong(new long[]{1337});
        testOperationsArrByte(new byte[]{42});
        testOperationsArrBoolean(new boolean[]{true,false});
        testOperationsArrDouble(new double[]{12.5});
        testOperationsArrFloat(new float[]{(float)5.6});

        /* Typing behaviour */
        testCheckType(new Tutu());
        testCastType(new Tata());

        /* Exception behaviour */
        testThrowCatch();

        /* Monitor behaviour */
        new Thread(new Runnable() {public void run() {testMonitor(2000);}}).start();
        new Thread(new Runnable() {public void run() {testMonitor(1);}}).start();
    }
}
