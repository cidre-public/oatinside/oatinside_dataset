package com.pg.bench.caes;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {

    static {
        System.loadLibrary("native-lib");
    }

    public static final int nb_run = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        long diffs[] = new long[nb_run];
        for(int n=0; n<nb_run;n++) {
            long startTime = 0;
            long difference = 0;

            startTime = System.nanoTime();
            aes128();
            difference = System.nanoTime() - startTime;

            //Log.i("bench", "["+n+"] took: " + difference + "ns" + " (" + difference / 1000000000. + "s)");
            Log.i("bench", ""+difference);
            diffs[n] = difference;
        }

        long avg = 0;
        for (long value : diffs) {
            avg += value;
        }
        avg /= diffs.length;

        Log.i("bench", "avg128: "+avg);
    }

    public native void aes128();
}