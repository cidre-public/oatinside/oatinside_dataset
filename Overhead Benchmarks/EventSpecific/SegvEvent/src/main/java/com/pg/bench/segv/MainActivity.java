package com.pg.bench.segv;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import java.util.Arrays;

public class MainActivity extends Activity {

    public static final int NB_RUN = 5;
    public static final int MAX_ACTION = 100;
    public static final int INCR_ACTION = 10;

    public long array[] = new long[MAX_ACTION+1];

    public void doActions(int nb_action) {
        for(int i=0;i<nb_action;i++) {
            array[i] = i;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for (int nb_action = 0; nb_action <= MAX_ACTION; nb_action += INCR_ACTION) {
            long diffs[] = new long[NB_RUN];
            for (int n = 0; n < NB_RUN; n++) {
                long startTime = 0;
                long difference = 0;

                startTime = System.nanoTime();
                doActions(nb_action);
                difference = System.nanoTime() - startTime;
                diffs[n] = difference;
            }

            long avg = 0;
            for (long value : diffs) {
                avg += value;
            }
            avg /= diffs.length;

            Log.i("bench", nb_action + " " + avg);
        }
    }
}
