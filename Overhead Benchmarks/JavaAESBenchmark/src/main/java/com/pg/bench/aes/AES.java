package com.pg.bench.aes;

/**
 * Main class for the AES 128-bit key encryption.
 * <p/>
 * I used this tutorial for the implementation details:
 * http://cboard.cprogramming.com/c-programming/87805-%5Btutorial%5D-implementing-advanced-encryption-standard.html
 * <p/>
 * Java bytes are signed so I work with chars instead.
 *
 * @author Johan Stenberg <jostenbe@kth.se>
 */
public class AES {

    /**
     * Encrypts the plaintext with the provided key and then
     * prints it.
     *
     * @param key       The key to be used.
     * @param plainText The plaintext to be encrypted.
     */
    public byte[] solve(char[] key, char[] plainText) {
        MathUtils mu = new MathUtils();
        mu.fillInitialStateMatrix(plainText);

        return mu.AES(key);
    }
}
