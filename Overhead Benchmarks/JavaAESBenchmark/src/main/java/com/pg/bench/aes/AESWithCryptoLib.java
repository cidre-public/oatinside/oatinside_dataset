package com.pg.bench.aes;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/*@Override
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        long diffs[] = new long[nb_run];
        for(int n=0; n<nb_run;n++) {
        long startTime = 0;
        long difference = 0;

        try {
final String myKey = "1234567890ABCDEF0123456789ABCDEF";
final String myIV = "89ABCDEF0123456789ABCDEF01234567";
final String myMsg = "Hello world! I am a lovely message waiting to be encrypted!";

final AESWithCryptoLib aesWithCryptoLib = new AESWithCryptoLib(
        AESWithCryptoLib.AESMode.CFB,
        AESWithCryptoLib.AESPad.NONE,
        128,
        myKey,
        myIV);

        startTime = System.nanoTime();
        for (long i = 0; i < 100; i++)
        aesWithCryptoLib.encrypt(myMsg);
        difference = System.nanoTime() - startTime;
        } catch (IllegalBlockSizeException e) {
        Log.i("bench", "Invalid block size");
        } catch (BadPaddingException e) {
        Log.i("bench", "Invalid padding");
        } catch (InvalidKeyException e) {
        Log.i("bench", "Invalid key");
        } catch (InvalidAlgorithmParameterException e) {
        Log.i("bench", "Invalid parameter");
        } catch (NoSuchPaddingException e) {
        Log.i("bench", "Unknown padding");
        } catch (NoSuchAlgorithmException e) {
        Log.i("bench", "Unknown algorithm");
        }

        //Log.i("bench", "["+n+"] took: " + difference + "ns" + " (" + difference / 1000000000. + "s)");
        Log.i("bench", ""+difference);
        diffs[n] = difference;
        }

        long avg = 0;
        for (long value : diffs) {
        avg += value;
        }
        avg /= diffs.length;


        // Rank order the values
        /*long[] v = new long[diffs.length];
        System.arraycopy(diffs, 0, v, 0, diffs.length);
        Arrays.sort(v);
        long q1 = diffs[(int) Math.round(v.length * 25 / 100)];
        long q3 = diffs[(int) Math.round(v.length * 75 / 100)];

        Log.i("bench", q1 + " " + avg + " " + q3);*/

        /*Log.i("bench", ""+avg);
        }*/

/**
 * <p>
 * This is a class designed to process AESWithCryptoLib messages sent from the LSL
 * implementation of AESWithCryptoLib which can be found here:<br/>
 * <a
 * href="https://wiki.secondlife.com/wiki/AES_LSL_Implementation">https://wiki
 * .secondlife.com/wiki/AES_LSL_Implementation</a>
 * </p>
 * <p>
 * This Java class will be updated to support the same modes of operation as the
 * LSL implementation. It currently assumes that keys and input-vectors are
 * processed as hex-strings, and that text is received as plain-text, while
 * ciphertext will be handled as base64 strings.
 * </p>
 *
 * @author Haravikk
 * @date Sep 15, 2008, 4:18:48 PM
 * @version 1.0
 */
public class AESWithCryptoLib {
    /** Our currently set block-cipher mode */
    protected AESMode mode = AESMode.CBC;
    /** Used to detect when a new {@link Cipher} Is needed. */
    protected boolean modeChanged = false;

    /** Our currently set padding mode */
    protected AESPad pad = AESPad.NONE;
    /** Our currently set pad-size */
    protected int padSize = 512;

    /** The currently loaded key */
    protected SecretKeySpec keySpec = null;
    /** The currently loaded input-vector */
    protected IvParameterSpec ivSpec = null;

    /** The currently active cipher */
    protected Cipher cipher = null;

    /** A random class for secure random operations. */
    protected Random random = new SecureRandom();

    /**
     * Creates an instance of an LSL compatible AESWithCryptoLib handler.
     *
     * @param mode
     *            the cipher-block mode of operation
     * @param pad
     *            the padding scheme to use
     * @param padSize
     *            the block-size to use when padding. Must be a non-zero,
     *            positive value that is a multiple of 128.
     * @param hexKey
     *            the key to start with (represented as hexadecimal string)
     * @param hexIV
     *            the input vector to start with (represented as hexadecimal
     *            string)
     * @throws NoSuchAlgorithmException
     *             if the AESWithCryptoLib algorithm is not supported by the current JVM
     * @throws NoSuchPaddingException
     *             if the padding scheme chosen is not supported by the current
     *             JVM
     */
    public AESWithCryptoLib(
            final AESMode mode,
            final AESPad pad,
            final int padSize,
            final String hexKey,
            final String hexIV)
            throws NoSuchAlgorithmException,
            NoSuchPaddingException {
        this.init(mode, pad, padSize, hexKey, hexIV);
    }

    /**
     * Decrypts a base64 ciphertext into plain-text
     *
     * @param base64ciphertext
     *            the ciphertext to decrypt
     * @return the plain-text that was originally encrypted
     * @throws InvalidKeyException
     *             if the currently loaded key is not valid
     * @throws InvalidAlgorithmParameterException
     *             if the AESWithCryptoLib algorithm is not supported by the current JVM
     * @throws IllegalBlockSizeException
     *             if the ciphertext is somehow unreadable (bad base64
     *             conversion)
     * @throws BadPaddingException
     *             if the chosen mode of operation requires padded data
     */
    public String decrypt(final String base64ciphertext)
            throws InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException {
        if (this.modeChanged) try {
            this.createCipher();
        } catch (final Exception e) { /* Do nothing */}

        this.cipher.init(Cipher.DECRYPT_MODE, this.keySpec, this.ivSpec);
        return new String(this.cipher.doFinal(Base64Coder
                .decode(base64ciphertext)));
    }

    /**
     * Encrypts plain-text into a base64 string
     *
     * @param text
     *            the plain-text to encrypt
     * @return the base64 ciphertext produced
     * @throws IllegalBlockSizeException
     *             if the plain text is somehow invalid
     * @throws BadPaddingException
     *             if the chosen mode of operation requires padded data
     * @throws InvalidKeyException
     *             if the currently loaded key is invalid
     * @throws InvalidAlgorithmParameterException
     *             if the AESWithCryptoLib algorithm is not supported by the current JVM
     */
    public String encrypt(final String text)
            throws IllegalBlockSizeException,
            BadPaddingException,
            InvalidKeyException,
            InvalidAlgorithmParameterException {
        if (this.modeChanged) try {
            this.createCipher();
        } catch (final Exception e) { /* Do nothing */}

        this.cipher.init(Cipher.ENCRYPT_MODE, this.keySpec, this.ivSpec);

        byte[] data = text.getBytes();
        int bits = data.length * 8;

        /* Apply padding */
        AESPad padding = this.pad;
        if (padding == AESPad.NONE) {
            if (this.mode == AESMode.CFB) { return Base64Coder
                    .encodeString(this.cipher.doFinal(data), 0, bits); }
            padding = AESPad.RBT;
        }

        int blockSize = this.padSize;
        if (padding == AESPad.RBT) blockSize = 128;

        final int blocks = bits / blockSize;
        int extra = bits % blockSize;

        if (padding == AESPad.RBT) {
            if (extra > 0) {
                /*
                 * This scheme takes the last encrypted block, encrypts it
                 * again, and XORs it with any leftover data, maintaining
                 * data-length. If input is less than a block in size, then the
                 * current input-vector is used.
                 */
                int bytes = extra / 8;
                if ((bytes * 8) < extra) ++bytes;

                // Grab leftover bytes
                final byte[] t = new byte[bytes];
                if (bytes > 0)
                    System.arraycopy(data, data.length - bytes, t, 0, bytes);

                // Encrypt all other data.
                byte[] lb;
                if (blocks < 1) {
                    // If not enough for a block, double-encrypt IV.
                    data = new byte[0];
                    lb =
                            this.cipher.doFinal(this.cipher.doFinal(this.ivSpec
                                    .getIV()));
                } else {
                    // If there are blocks, then double-encrypt final one.
                    data = this.cipher.doFinal(data, 0, data.length - bytes);
                    lb = this.cipher.doFinal(data, data.length - 16, 16);
                }

                // XOR lb with t.
                for (int i = 0; i < t.length; ++i)
                    t[i] ^= lb[i];

                lb = new byte[data.length + t.length];
                System.arraycopy(data, 0, lb, 0, data.length);
                System.arraycopy(t, 0, lb, data.length, t.length);

                return Base64Coder.encodeString(lb);
            }
            return Base64Coder.encodeString(this.cipher.doFinal(data), 0, bits);
        }

        // Padding schemes that add bytes until block-boundary is reached.
        extra = blockSize - extra;

        if (padding == AESPad.NULLS_SAFE) {
            ++bits;
            final int bytes = bits / 8;
            final int bit = bytes % 8;

            if (bytes < data.length) data[bytes] |= (1 << (8 - bit));
            else {
                final byte[] t = new byte[data.length + 1];
                System.arraycopy(data, 0, t, 0, data.length);
                t[data.length] = (byte) 0x80;
                data = t;
            }

            if ((--extra) < 0) extra += blockSize;
            padding = AESPad.NULLS;
        }

        int bytes = extra / 8;
        if (bytes <= 0) {
            if (padding == AESPad.NULLS)
                return Base64Coder.encodeString(
                        this.cipher.doFinal(data),
                        0,
                        bits);

            bytes = blockSize / 8;
            extra += blockSize;
        }

        bits += extra;
        final byte[] t = new byte[data.length + bytes];
        int i = data.length;
        System.arraycopy(data, 0, t, 0, data.length);
        data = t;

        for (; i < data.length; ++i) {
            byte b = 0;
            if ((i >= (data.length - 4)) && (padding != AESPad.NULLS)) b =
                    (byte) bytes;
            else if (padding == AESPad.RANDOM)
                b = (byte) this.random.nextInt(256);

            data[i] = b;
        }

        return Base64Coder.encodeString(this.cipher.doFinal(data), 0, bits);
    }

    /**
     * Initialises this AESWithCryptoLib instance with a mode, pad, key, and input vector in
     * a single operation
     *
     * @param mode
     *            the cipher-block mode of operation
     * @param pad
     *            the padding scheme to use
     * @param padSize
     *            the block-size to use when padding. Must be a non-zero,
     *            positive value that is a multiple of 128.
     * @param hexKey
     *            the key to use as a hexadecimal string
     * @param hexIV
     *            the input-vector to use as a hexadecimal string
     * @throws NoSuchAlgorithmException
     *             if the AESWithCryptoLib algorithm is not supported by the current JVM
     * @throws NoSuchPaddingException
     *             if the padding method is not supported by the current JVM
     */
    public void init(
            final AESMode mode,
            final AESPad pad,
            final int padSize,
            final String hexKey,
            final String hexIV)
            throws NoSuchAlgorithmException,
            NoSuchPaddingException {
        if ((mode == null) || (pad == null) || (hexKey == null) ||
                (hexIV == null))
            throw new IllegalArgumentException("No arguments may be null");

        this.setMode(mode);
        this.setPad(pad, padSize);
        this.setKey(hexKey);
        this.setInputVector(hexIV);

        this.random.nextInt();

        this.createCipher();
    }

    /**
     * Sets the input-vector for this engine to use
     *
     * @param hexIV
     *            a hexadecimal input-vector to use
     */
    public void setInputVector(final String hexIV) {
        if (hexIV == null)
            throw new IllegalArgumentException("Input-vector may not be null!");

        this.ivSpec = new IvParameterSpec(HexCoder.hexToBytes(hexIV));
    }

    /**
     * Sets the key for this engine to use
     *
     * @param hexKey
     *            a hexadecimal key to use
     */
    public void setKey(final String hexKey) {
        if (hexKey == null)
            throw new IllegalArgumentException("Key may not be null!");

        this.keySpec = new SecretKeySpec(HexCoder.hexToBytes(hexKey), "AESWithCryptoLib");
    }

    /**
     * Sets the mode of this implementation
     *
     * @param mode
     *            the mode to set
     */
    public void setMode(final AESMode mode) {
        if (mode == null)
            throw new IllegalArgumentException("Mode may not be null!");

        this.mode = mode;
        this.modeChanged = true;
    }

    /**
     * Sets the padding scheme of this implementation
     *
     * @param pad
     *            the padding scheme to use
     */
    public void setPad(final AESPad pad) {
        this.setPad(pad, this.padSize);
    }

    /**
     * Sets the padding scheme of this implementation
     *
     * @param pad
     *            the padding scheme to use
     * @param padSize
     *            the block-size to use when padding. Must be a non-zero,
     *            positive value that is a multiple of 128.
     */
    public void setPad(final AESPad pad, final int padSize) {
        if (pad == null)
            throw new IllegalArgumentException("Pad may not be null!");
        if ((padSize <= 0) || ((padSize % 128) > 0))
            throw new IllegalArgumentException(
                    "Pad size may not be less than zero, and must be a multiple of 128");

        this.pad = pad;
        this.padSize = padSize;
    }

    /**
     * Creates a new cipher instance for processing
     *
     * @throws NoSuchPaddingException
     *             if the padding scheme set is invalid
     * @throws NoSuchAlgorithmException
     *             if AESWithCryptoLib is not supported by this JVM
     */
    protected void createCipher()
            throws NoSuchAlgorithmException,
            NoSuchPaddingException {
        this.cipher = Cipher.getInstance("AESWithCryptoLib/" + this.mode + "/NoPadding");
    }

    /** Defines modes of operation combatible with LSL */
    public enum AESMode {
        /** Cipher-Block-Chaining mode */
        CBC,
        /** Cipher FeedBack mode */
        CFB;
    }

    /** Defines padding schemes compatible with LSL */
    public enum AESPad {
        /** Performs no padding, will switch to RBT if mode is CBC. */
        NONE,
        /**
         * Enables CFB mode temporarily for the final complete block, and
         * combines with data. This preserves data-length.
         */
        RBT,
        /**
         * Adds null-bytes to the end of the data until it is of correct-size.
         * This is an padding scheme (may result in loss of null-bytes from
         * original data).
         */
        NULLS,
        /**
         * Same as NULLS, except that it first appends a single '1' bit to the
         * data before padding.
         */
        NULLS_SAFE,
        /**
         * Appends null-bytes to the data until one word from block-size, final
         * word is then populated with bytes describing the number of padding
         * bytes added.
         */
        ZEROES,
        /**
         * Same as ZEROES, except that random-bytes are used in place of
         * null-bytes.
         */
        RANDOM;
    }
}