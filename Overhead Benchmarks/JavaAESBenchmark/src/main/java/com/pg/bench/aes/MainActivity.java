package com.pg.bench.aes;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.InputStream;

public class MainActivity extends Activity {

    public static final int nb_run = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        char[] key = {'P','4','s','$','W','0','r','d','d','r','0','W','$','s','4','P'};
        char[] input = {'T','e','s','t','i','n','g',' ','A','E','S','1','2','8','!','!'};
        byte ret[] = {};

        long diffs[] = new long[nb_run];
        for(int n=0; n<nb_run;n++) {
            long startTime = 0;
            long difference = 0;
            AES aes = new AES();

            startTime = System.nanoTime();
            ret = aes.solve(key, input);
            difference = System.nanoTime() - startTime;

            //Log.i("bench", "["+n+"] took: " + difference + "ns" + " (" + difference / 1000000000. + "s)");
            Log.i("bench", ""+difference);
            diffs[n] = difference;
        }

        long avg = 0;
        for (long value : diffs) {
            avg += value;
        }
        avg /= diffs.length;


        // Rank order the values
        /*long[] v = new long[diffs.length];
        System.arraycopy(diffs, 0, v, 0, diffs.length);
        Arrays.sort(v);
        long q1 = diffs[(int) Math.round(v.length * 25 / 100)];
        long q3 = diffs[(int) Math.round(v.length * 75 / 100)];

        Log.i("bench", q1 + " " + avg + " " + q3);*/

        Log.i("bench", ""+avg);
        Log.i("bench", ""+ret);
    }
}
