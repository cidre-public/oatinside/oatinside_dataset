This repository is an Android Studio project which contains:

- Under `Dalvik Behaviors` module: two applications (one in Java, one in C) that perform all possible unitary behaviors allowed by the DVM.

- Under `Overhead Benchmark` module: two applications that implement AES (one in Java, one in C) and a sub-module `EventSpecific` which contains 3 applications that generates respectively `Direct`, `Trap` or `SEGV` events.

- `SimpleTestPIN`: A simple example application that checks the value of an entered PIN.