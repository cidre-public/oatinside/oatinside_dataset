#include <jni.h>

#include <android/log.h>
#define APPNAME "CRC32"

// #define OFFSET_OF_OFFSET_FIELD_IN_ARTFIELD_CLASS 3
#define PIN_FIELD_OFFSET 0x8

extern "C"
JNIEXPORT void JNICALL
Java_pg_testpin_SimpleTestPIN_set_1pin(JNIEnv *env, jobject thisObj, jint pin) {
    /* How to retrieve the offset: */
    /*jclass thisCls = env->GetObjectClass(thisObj);
    jfieldID fid = env->GetFieldID(thisCls, "pin", "I");
    unsigned int offset = *((unsigned int*)fid+OFFSET_OF_OFFSET_FIELD_IN_ARTFIELD_CLASS);
    __android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "Field is 0x%lx", offset);*/

    unsigned int* thisPtr = (unsigned int*) *(unsigned int*)thisObj;
    unsigned int* field_ptr = &thisPtr[PIN_FIELD_OFFSET/4];

    // __android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "Field addr is %p", field_ptr);

    *field_ptr = pin;
    *field_ptr += 7331;
}