package pg.testpin;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    public SimpleTestPIN pin = new SimpleTestPIN();

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bt1 = (Button) findViewById(R.id.button);
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv = (TextView) findViewById(R.id.textView);
                EditText pinview = (EditText) findViewById(R.id.editText);

                int entered_pin=-1;
                try {
                    entered_pin = Integer.parseInt(pinview.getText().toString());

                    pin.set_pin(entered_pin);
                } catch (NumberFormatException e) {
                    tv.setText("Incorrect format");
                }

                try {
                    pin.test();
                    if(pin.validated)
                        tv.setText("Good PIN");
                    else
                        tv.setText("Wrong PIN");
                } catch (Exception e) {
                    tv.setText("Incorrect format");
                }
            }
        });
    }
}
