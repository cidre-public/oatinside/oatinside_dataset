package pg.testpin;

public class SimpleTestPIN {
    public int pin = -1;
    public boolean validated = true;

    public void test() throws Exception {
        if (this.pin < 7331)
            throw new Exception("Negative PIN");

        if ((this.pin ^ 0x2323) == 0x2ff) // (1337+7331) ^ 0x2323 = 1337 ^ 0x2323 = 0x2ff

            validated = true;
        else
            validated = false;
    }

    public native void set_pin(int pin);
}
